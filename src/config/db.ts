export {}
const dotenv = require('dotenv').config()
const Pool = require('pg').Pool
const pool = new Pool({
	HOST: process.env.PGHOST,
	USER: process.env.PGUSER,
	PASSWORD: process.env.PGPASSWORD,
	DB: process.env.PGDATABASE,
	PORT: process.env.PGPORT,
})
module.exports = pool
