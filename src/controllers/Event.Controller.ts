export {}
const db = require('../config/db')
class EventController {
	private nbEventsPerPage: number = parseInt(process.env.NB_ELEMENTS_PER_PAGE)

	//get all events.
	async getEvents(request: { page: number; events: [] }) {
		let result: any = {
			results: {},
			rows: 0,
			total: 0,
		}
		let query
		let queryCount
		let paged: number
		let limit: number

		if (!request) return false

		//No Paramaters
		if (Object.keys(request).length === 0) {
			query = await db.query(`SELECT * FROM events`)
			queryCount = await db.query(`SELECT COUNT(*) FROM events`)
		}

		//Page
		if (request.page) {
			limit = request.page * this.nbEventsPerPage
			result.offset = limit - this.nbEventsPerPage
			result.page = request.page
			query = await db.query(
				`SELECT * FROM events ORDER BY created ASC LIMIT $1 OFFSET $2`,
				[limit, result.offset]
			)
			queryCount = await db.query(`SELECT COUNT(*) FROM events`)
		}

		//Events
		if (request.events) {
			let listEvents = request.events
			query = await db
				.query(`SELECT * FROM events WHERE id = ANY ($1)`, [listEvents])
				.catch((e: Error) => {
					throw new Error(e.message)
				})
			queryCount = await db.query(
				`SELECT COUNT(*) FROM events WHERE id = ANY ($1)`,
				[listEvents]
			)
		}
		if (query.rows) {
			result.results = query.rows
			result.rows = query.rowCount
			result.total = parseInt(queryCount.rows[0].count)
			if (paged) {
				result.offset = paged
			}
		}
		return result
	}

	//Get a event
	async getEvent(eventId: string) {
		let results = await db
			.query(`SELECT * FROM events WHERE id=$1`, [parseInt(eventId)])
			.catch()
		if (results.rows.length > 0) {
			return results.rows[0]
		} else {
			return false
		}
	}
	//create a event.
	async createEvent(event: any) {
		let created = new Date().toISOString()
		const newEvent = await db
			.query(
				'INSERT INTO events (title, description, allday, start, "end", created) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *',
				[
					event.title,
					event.description,
					event.allday,
					event.start,
					event.end,
					created,
				]
			)
			.catch((e: any) => {})

		if (newEvent.rows[0]) {
			return newEvent.rows[0]
		} else {
			return false
		}
	}

	//Update a event
	async updateEvent(event: any, data: any) {
		const eventId = event.id
		const newData = { ...event, ...data }

		const response = await db
			.query(
				`UPDATE events SET title=$2, description=$3, allday=$4, start=$5, "end"=$6 WHERE id=$1 RETURNING *`,
				[
					parseInt(eventId),
					newData.title,
					newData.description,
					newData.allday,
					newData.start,
					newData.end,
				]
			)
			.catch()
		if (response.rows[0]) {
			return response.rows[0]
		} else {
			return false
		}
	}

	/** Event : Register a user
	 * @param {uuid} user The User being registered
	 * @returns {object} The event
	 */
	async registerUser(params: { event_id: number; user_id: string }) {
		const { event_id, user_id } = params

		//Check if user exists in array
		const exists = await db
			.query(`SELECT * FROM events WHERE $2 = ANY(users) AND id = $1`, [
				event_id,
				user_id,
			])
			.catch((e: Error) => {})
		let response
		if (exists.rows[0]) {
			response = await db.query(
				`UPDATE events SET users = array_remove(users, $2) WHERE id=$1 RETURNING *`,
				[event_id, user_id]
			)
		} else {
			response = await db.query(
				`UPDATE events SET users=array_append(users, $2) WHERE id=$1 RETURNING *`,
				[event_id, user_id]
			)
		}

		if (response.rows[0]) {
			return response.rows[0]
		} else {
			return false
		}
	}

	//delete a event.
	async deleteEvent(eventId: string) {
		//delete event
		const result = await db
			.query(`DELETE FROM events WHERE id=$1 RETURNING *`, [
				parseInt(eventId),
			])
			.catch()

		if (result.rowCount > 0) {
			return result
		} else {
			return false
		}
	}
}

module.exports = EventController
