import LikeModel from '../entities/Like.Entity'
import LikeServicesModel from '../models/like/Like.Services.Model'
interface LikeControllerConstructor {
	new (services: LikeServicesModel): LikeServicesModel
}
interface LikeControllerModel {
	addLike(like: LikeModel): Promise<LikeModel | Error>
	getLikes(request: {
		user_id: string
		liked: boolean
	}): Promise<LikeModel[] | Error>
}

export { LikeControllerConstructor, LikeControllerModel }

/**
 * Like Controller
 */
class LikeController {
	services: LikeServicesModel

	constructor(services: LikeServicesModel) {
		this.services = services
	}
	//Get Likes / Unliked Likes
	async getLikes(request: {
		user_id: string
		product_id: number
		liked: boolean
	}): Promise<LikeModel[] | Error> {
		try {
			let result: any = {
				results: {},
				rows: 0,
				total: 0,
			}
			const query = await this.services.getLikes(request)
			if (query.rows) {
				result.results = query.rows
				result.rows = query.rowCount
				result.total = query.rows.length
			}
			return query
		} catch (e) {
			return e
		}
	}

	//create a like
	async createLike(like: LikeModel) {
		try {
			const result = await this.services.addLike(like)
			return result
		} catch (e) {
			return e
		}
	}
}

module.exports = LikeController
