export {}
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
import UserModel from '../entities/User.Entity'
import UserServicesModel from '../models/user/User.Services.Model'
interface UserControllerConstructor {
	new (services: UserServicesModel): UserServicesModel
}
interface UserControllerModel {
	services: UserServicesModel
	nbUsersPerPage: number
	getUser(params: {
		user_id?: string
		username?: string
		email?: string
	}): Promise<UserModel | Error>
	getUsers(request: {
		page: number
		users: number[]
	}): Promise<UserModel[] | Error>
	createUser(user: UserModel): Promise<UserModel | Error>
	updateUser(user: any, data: any): Promise<UserModel | Error>
	deleteUser(user_id: string): Promise<boolean | Error>
}

export { UserControllerConstructor, UserControllerModel }
/**
 * User Controller
 */
class UserController {
	nbUsersPerPage: number = parseInt(process.env.NB_ELEMENTS_PER_PAGE)
	services: UserServicesModel

	constructor(services: UserServicesModel) {
		this.services = services
	}

	/**
	 * Get the User from its ID, its username or its email
	 * @param {object} params : User Details for getting the User Object
	 * @param {string} params.user_id : The ID of the User
	 * @param {string} params.username : The Username of the User
	 * @param {string} params.email: The Email Of the User
	 * @returns {UserModel} : User Object
	 */
	async getUser(params: {
		user_id?: string
		username?: string
		email?: string
	}): Promise<UserModel | Error> {
		const user = await this.services.getUser(params)
		if (user) {
			return user
		} else {
			return new Error('No User Found !')
		}
	}
	/**
	 * Activate User
	 * @param {UserModel.id} user_id : User Object you want to update
	 * @returns {UserModel} : User Object updated
	 */
	async activateUser(user_id: string): Promise<UserModel | Error> {
		const user = await this.services.activateUser(user_id)
		if (user) {
			return user
		} else {
			return new Error('No User Found !')
		}
	}

	/**
	 * Fetch User Password
	 * @param {UserModel.id} user_id : User ID you want to update
	 * @returns {UserModel.password} Token Generated
	 */
	async getUserPassword(user_id: string): Promise<string> {
		try {
			const user = await this.services.getUser({ user_id: user_id })
			return user.password
		} catch (e) {}
	}

	/**
	 * Get Users
	 * @param request : Request parameters for querying users
	 * @param request.page : The Page of the Users List
	 * @param request.users : Specific Users you want to get
	 *
	 * @returns : A list of Users Objects
	 */
	async getUsers(request: { page: number; users: number[] }) {
		let result: any = {
			results: {},
			rows: 0,
			total: 0,
		}
		let params = {}
		let paramsCount = {}
		let users = []
		let usersCount = 0

		//Page
		if (request.page) {
			let limit: number = request.page * this.nbUsersPerPage
			let offset = (result.offset = limit - this.nbUsersPerPage)
			result.page = request.page
			params = {
				page: request.page,
				limit: limit,
				offset: offset,
			}
		}

		//Users
		if (request.users) {
			let listUsers = request.users
			params = {
				users: request.users,
			}
			paramsCount = {
				users: request.users,
			}
		}

		//Services
		users = await this.services.getUsers(params)
		usersCount = await this.services.countUsers(paramsCount)

		result.results = users
		result.rows = users.length
		result.total = usersCount
		//Total
		if (usersCount) {
			result.total = usersCount
		} else {
			result.total = users.length
		}
		return result
	}

	/**
	 * Creating a user
	 * @param {UserModel} User Details for Registration
	 * @returns {UserModel} User Object
	 */
	async registerUser(user: UserModel): Promise<UserModel | Error> {
		try {
			let { username, name, email, password, role } = user
			if (!email) {
				throw {
					code: 'auth/missing-email',
					message: 'Missing email',
					error: new Error(),
				}
			}
			if (!password) {
				throw {
					code: 'auth/missing-password',
					message: 'Missing password',
					error: new Error(),
				}
			}

			if (!role) {
				role = process.env.DEFAULT_ROLE
			}
			const newUser = await this.services.addUser(user)
			return newUser
		} catch (e) {
			return e
		}
	}

	/**
	 * Login User Process
	 * @param {string} email : Email of the User
	 * @param {password} password : Password of the User
	 * @returns {UserModel} User Object of the user logged in
	 */
	async loginUser(
		email: string,
		password: string
	): Promise<UserModel | Error> {
		try {
			const user = await this.services.getUser({ email: email })
			if (user && user.password) {
				const isMatch = await bcrypt.compare(password, user.password)
				if (!isMatch) {
					throw {
						code: 'auth/unable-login',
						message: 'Wrong Email or Password. Please retry again.',
					}
				}
				delete user.password
				return user
			} else {
				throw {
					code: 'auth/unable-login',
					message: 'Wrong Email or Password. Please retry again.',
				}
			}
		} catch (e) {
			throw {
				code: e.code,
				message: e.message,
				error: e,
			}
		}
	}

	/**
	 * Update a User
	 * @param {UserModel} user : User Object you want to update
	 * @param {UserModel} data : Data of the user you want to change
	 * @returns {UserModel} : User Object updated
	 */
	async updateUser(
		user: UserModel,
		data: UserModel
	): Promise<UserModel | Error> {
		try {
			const updatedUser = await this.services.updateUser(user, data)
			return updatedUser
		} catch (e) {}
	}

	/**
	 * Update a user password
	 * @param {UserModel} user : User Object you want to update
	 * @param newPassword : New Password
	 * @returns {UserModel} : User Object updated
	 */
	async updateUserPassword(
		user: any,
		newPassword: string
	): Promise<UserModel | Error> {
		try {
			newPassword = await bcrypt.hash(newPassword, 8)
			const newData = { ...user, ...{ password: newPassword } }
			const updatedUser = await this.services.updateUser(user, newData)
			return updatedUser
		} catch (e) {
			return new Error('Something went wrong while updating the password')
		}
	}

	/**
	 * Generate an Auth Token for a user
	 * @param {UserModel} user : User Object you want to update
	 * @returns {string} Token Generated
	 */
	async generateAuthToken(user: UserModel): Promise<string> {
		const token = jwt.sign(
			{
				_id: user.id,
				_role: user.role,
			},
			process.env.TOKEN_KEY,
			{ expiresIn: '1d' }
		)
		return token
	}

	/**
	 * Generate Lost Password Token
	 * @param {UserModel.id} user_id : User ID you want to update
	 * @returns {string} Token Generated
	 */
	async generateToken(user_id: number): Promise<string> {
		const token = jwt.sign(
			{
				_id: user_id,
			},
			process.env.TOKEN_KEY,
			{ expiresIn: '1h' }
		)

		const decoded = await jwt.verify(token, process.env.TOKEN_KEY)

		return token
	}

	/**
	 * Delete a user.
	 * @param {UserModel.id} user_id : User ID you want to update
	 * @returns {boolean} : Return a success or failure
	 */
	async deleteUser(user_id: string): Promise<boolean | Error> {
		try {
			let result = await this.services.deleteUser(user_id)
			return result
		} catch (e) {
			return e
		}
	}
}

module.exports = UserController
