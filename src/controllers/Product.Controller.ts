import ProductModel from '../entities/Product.Entity'
import ProductServicesModel from '../models/product/Product.Services.Model'
interface ProductControllerConstructor {
	new (services: ProductServicesModel): ProductServicesModel
}
interface ProductControllerModel {
	nbProductsPerPage: number
	getProduct(product_id: string): Promise<ProductModel | Error>
	getProducts(request: {
		page: number
		products: []
		user_id: string
	}): Promise<ProductModel[] | Error>
	createProduct(product: ProductModel): Promise<ProductModel | Error>
	updateProduct(product: any, data: any): Promise<ProductModel | Error>
	deleteProduct(product_id: string): Promise<boolean | Error>
}

export { ProductControllerConstructor, ProductControllerModel }

/**
 * Product Controller
 */
class ProductController {
	private nbProductsPerPage: number = parseInt(
		process.env.NB_ELEMENTS_PER_PAGE
	)
	private services = {
		addProduct: Function(),
		countProducts: Function(),
		deleteProduct: Function(),
		getProduct: Function(),
		getProducts: Function(),
		updateProduct: Function(),
	}

	constructor(services: any) {
		this.nbProductsPerPage = parseInt(process.env.NB_ELEMENTS_PER_PAGE)
			? parseInt(process.env.NB_ELEMENTS_PER_PAGE)
			: 25
		this.services = services
	}

	/**
	 * Get a specfic Product
	 * @param {ProductModel.id} productId ID of the product for the query
	 * @returns Product Object
	 */
	async getProduct(product_id: string): Promise<ProductModel | Error> {
		const product = await this.services.getProduct(product_id)
		if (product) {
			return product
		} else {
			return new Error('No Product Found !')
		}
	}

	/**
	 * Get All Products
	 * @param {object} params : Query parameters for getting products
	 * @param {number} params.page : Query a specific page of products
	 * @param {array} params.products : Query specific products
	 * @param {string} params.user_id : Query products of a a specific User
	 * @returns List of Products in Array
	 */
	async getProducts(request: {
		page: number
		products: []
		user_id: string
	}): Promise<ProductModel[] | Error> {
		let result: any = {
			results: {},
			rows: 0,
			total: 0,
		}
		let params = {}
		let paramsCount = {}
		if (!params) {
			return new Error('No request(s) enetered !')
		}

		//Page
		if (request.page) {
			let limit: number = request.page * this.nbProductsPerPage
			let offset = (result.offset = limit - this.nbProductsPerPage)
			result.page = request.page
			params = {
				page: request.page,
				limit: limit,
				offset: offset,
			}
		}

		//User
		if (request.user_id) {
			params = {
				user_id: request.user_id,
			}
			paramsCount = {
				user_id: request.user_id,
			}
		}

		//Services
		const products = await this.services.getProducts(params)
		const productsCount = await this.services.countProducts(params)

		//Building Result
		result.results = products
		result.rows = products.length
		result.total = productsCount
		//Total
		if (productsCount) {
			result.total = productsCount
		} else {
			result.total = products.length
		}
		return result
	}

	/**
	 * Create A Product
	 * @param {ProductModel} product Product Object for creation
	 * @returns Product Object newly created
	 */
	async createProduct(product: ProductModel): Promise<ProductModel | Error> {
		try {
			const newProduct = await this.services.addProduct(product)
			return newProduct
		} catch (e) {
			return new Error(e)
		}
	}

	/**
	 * Update A Product
	 * @param {ProductModel} product Product Object for identificating the product to update
	 * @param {ProductModel} data Product Information needed for update
	 * @returns Product Object updated
	 */
	async updateProduct(
		product: any,
		data: any
	): Promise<ProductModel | Error> {
		try {
			const updatedProduct = await this.services.updateProduct(
				product,
				data
			)
			return updatedProduct
		} catch (e) {
			return new Error(e)
		}
	}

	/**
	 * Delete a specific product
	 * @param {number} product_id The ID of the product that has to be deleted
	 * @returns Product object deleted
	 */
	async deleteProduct(product_id: number): Promise<boolean | Error> {
		try {
			let result = await this.services.deleteProduct(product_id)
			return result
		} catch (e) {
			return e
		}
	}
}

module.exports = ProductController
