const express = require('express')
const router = express.Router()
const fileUpload = require('express-fileupload')
const dotenv = require('dotenv').config()
const cors = require('cors')
const helmet = require('helmet')
const app = express()
const rateLimit = require('express-rate-limit')

//Rate Limit : Brute Force Protection
const limiter = rateLimit({
	windowMs: 15 * 60 * 1000, // 15 minutes
	max: 100, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
	standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
	legacyHeaders: false, // Disable the `X-RateLimit-*` headers
})
// app.use('/api', limiter)
app.use(express.json())

app.use(
	helmet({
		contentSecurityPolicy: true,
		crossOriginEmbedderPolicy: true,
		crossOriginOpenerPolicy: true,
		crossOriginResourcePolicy: { policy: 'same-site' },
		dnsPrefetchControl: true,
		expectCt: true,
		frameguard: true,
		hidePoweredBy: true,
		hsts: true,
		ieNoOpen: true,
		noSniff: true,
		originAgentCluster: true,
		permittedCrossDomainPolicies: true,
		referrerPolicy: true,
		xssFilter: true,
	})
)
//CORS
var corsOptions = {
	origin: process.env.APP_URL,
	optionsSuccessStatus: 200, // For legacy browser support
}
app.use(cors(corsOptions))
app.use(fileUpload())
app.use(
	express.urlencoded({
		extended: true,
	})
)

app.set('view engine', 'ejs')
app.set('views', 'src/views')

//Static Resources
app.use('/static', express.static(`${__dirname}/public`))

//Routes
app.use(require('./routes'))

//Error Handling
app.use(function (err: any, req: any, res: any, next: any) {
	res.status(500).send({
		status: 500,
		code: err.code,
		message: err.message,
	})
})

//Port
const PORT = process.env.SERVER_PORT || 4000

app.listen(PORT, () => {
	console.log(`app started on port ${PORT}`)
})
