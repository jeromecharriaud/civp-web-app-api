const userRoles = {
	administrator: {
		product: {
			read: true,
			create: true,
			delete: true,
			update: true,
		},
		event: {
			read: true,
			create: true,
			delete: true,
			update: true,
		},
		user: {
			read: true,
			create: true,
			delete: true,
			update: true,
		},
		like: {
			read: true,
			create: true,
			delete: true,
			update: true,
		},
		file: {
			read: true,
			create: true,
			delete: true,
			update: true,
		},
		email: {
			read: true,
			create: true,
			delete: true,
			update: true,
		},
	},
	subscriber: {
		product: {
			read: true,
			create: false,
			delete: false,
			update: false,
		},
		event: {
			read: true,
			create: false,
			delete: false,
			update: false,
		},
		user: {
			read: true,
			create: false,
			delete: false,
			update: true,
		},
		like: {
			read: true,
			create: true,
			delete: false,
			update: false,
		},
		file: {
			read: false,
			create: true,
			delete: false,
			update: false,
		},
		email: {
			read: false,
			create: true,
			delete: false,
			update: false,
		},
	},
}

module.exports = userRoles
