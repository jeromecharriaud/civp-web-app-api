var path = require('path')

const root = path.resolve('./src')

const paths = {
	root: root,
	images: root + '\\public\\images',
}
module.exports = paths
