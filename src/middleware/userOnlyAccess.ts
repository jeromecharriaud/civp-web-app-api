//Models
import { Request, Response, NextFunction } from 'express'
const jwt = require('jsonwebtoken')

const userOnlyAccess = (UserController: any) => {
	return async (req: Request, res: Response, next: NextFunction) => {
		try {
			let { user_id, email } = req.body

			const token = req.header('Authorization').replace('Bearer ', '')
			const decoded = jwt.verify(token, process.env.TOKEN_KEY)
			const role = decoded._role
			//Exclude admin from the restriction
			if (role === 'administrator') {
				next()
			}
			//Normal User
			else {
				//No Email or No User Id
				if (email === undefined && user_id === undefined) {
					throw {
						code: 'userOnlyAccess/no-email-or-id-provided',
						message: 'No Email or ID Provided',
						error: new Error(),
					}
				}
				//Email but no user Id
				if (email && !user_id) {
					const user = await new UserController().getUser({
						email: email,
					})
					user_id = user.id
				}
				if (user_id !== parseInt(decoded._id)) {
					throw {
						code: 'userOnlyAccess/access-to-data-of-another-user',
						message:
							'Permission Denied : Trying to access to data of another user',
						error: new Error(),
					}
				}
				next()
			}
		} catch (error) {
			next(error)
		}
	}
}

module.exports = userOnlyAccess
