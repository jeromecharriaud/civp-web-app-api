//Models
import { Request, Response, NextFunction } from 'express'
import CustomRequest from '../models/Request.Model'

const checkEmail = (UserController: any) => {
	return async (req: CustomRequest, res: Response, next: NextFunction) => {
		try {
			const email = req.body.email
			const user = await new UserController({
				getUser: require('../services/user/getUser'),
			}).getUser({
				email: email,
			})

			if (!user || !email) {
				throw {
					code: 'user/user-does-not-exist',
					message: 'User does not exist. Please enter a new email.',
					error: new Error(),
				}
			}

			req.user = user
			next()
		} catch (error) {
			next(error)
		}
	}
}

module.exports = checkEmail
