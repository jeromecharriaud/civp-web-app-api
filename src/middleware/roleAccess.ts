//Models
import { Request, Response, NextFunction } from 'express'
const capacities = require('../roles/')
const jwt = require('jsonwebtoken')

const grantRoleAccess = (model: string, capacity: string) => {
	return async (req: Request, res: Response, next: NextFunction) => {
		let token
		if (req.header('Authorization')) {
			token = req.header('Authorization').replace('Bearer ', '')
		}
		if (!token) {
			throw new Error('No Token')
		}

		const decoded = jwt.verify(token, process.env.TOKEN_KEY)
		const role = decoded._role
		try {
			if (role === 'all') next()
			if (!role || !model || !capacity) {
				throw {
					code: 'role/capacity-does-not-exist',
					message: 'Error in granting access.',
					error: new Error(),
				}
			}
			//Check the capacity of the user to do the specific action
			if (capacities[role][model][capacity] === true) {
				next()
			} else {
				throw {
					code: 'role/permission-denied',
					message: "You don't have the right to do this.",
					error: new Error(),
				}
			}
		} catch (error) {
			next(error)
		}
	}
}

module.exports = grantRoleAccess
