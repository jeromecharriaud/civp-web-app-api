const jwt = require('jsonwebtoken')

//Models
import { Request, Response, NextFunction } from 'express'
import CustomRequest from '../models/Request.Model'

const verifyToken = (UserController: any) => {
	return async (req: CustomRequest, res: Response, next: NextFunction) => {
		try {
			let token
			if (req.header('Authorization')) {
				token = req.header('Authorization').replace('Bearer ', '')
			}

			if (!token) {
				throw new Error('No Token')
			}

			const decoded = jwt.verify(token, process.env.TOKEN_KEY)
			const getUser = require('../services/user/getUser')
			const user = await new UserController({
				getUser: getUser,
			}).getUser({
				user_id: decoded._id,
			})

			if (!user) {
				throw {
					code: 'auth/no-user',
					message: "Invalid Token : User can't be found",
					error: new Error(),
				}
			}
			req.user = user
			next()
		} catch (error) {
			if (error.message === 'jwt expired') {
				error = new Error(
					'Your session has expired. Please Login Again.'
				)
			}
			next(error)
		}
	}
}

module.exports = verifyToken
