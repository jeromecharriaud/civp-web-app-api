export {}
const db = require('../config/db')
const fs = require('fs')
const dataProducts = fs.readFileSync('src/sql/productsData.sql').toString()
const dataUsers = fs.readFileSync('src/sql/usersData.sql').toString()
const dataLikes = fs.readFileSync('src/sql/likesData.sql').toString()

//Products
const importProducts = async () => {
	return await new Promise((resolve, reject) => {
		db.query(
			'ALTER SEQUENCE products_ID_seq RESTART WITH 1',
			(err: any, res: any) => {
				db.query('DELETE FROM products', (err: any, res: any) => {
					db.query(dataProducts, (err: any, res: any) => {
						resolve('resolved')
					})
				})
			}
		)
	})
}

//Users
const importUsers = async () => {
	return await new Promise((resolve, reject) => {
		db.query(
			'ALTER SEQUENCE users_ID_seq RESTART WITH 1',
			(err: any, res: any) => {
				db.query('DELETE FROM users', (err: any, res: any) => {
					// db.query(dataUsers, (err: any, res: any) => {
					// 	resolve(dataUsers)
					// })
				})
			}
		)
	})
}

//LIKES
const importLikes = async () => {
	return await new Promise((resolve, reject) => {
		db.query(
			'ALTER SEQUENCE likes_ID_seq RESTART WITH 1',
			(err: any, res: any) => {
				db.query('DELETE FROM likes', (err: any, res: any) => {
					db.query(dataLikes, (err: any, res: any) => {
						resolve(dataLikes)
					})
				})
			}
		)
	})
}

const importData = async () => {
	// const users = await importUsers()
	const products = await importProducts()
	const likes = await importLikes()

	await Promise.all([products, likes])
		.then(() => {})
		.catch(() => {})
		.finally(() => {
			db.end()
		})
}

importData()
