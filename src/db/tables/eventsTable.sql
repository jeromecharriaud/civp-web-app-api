DROP TABLE IF EXISTS events;

CREATE TABLE events (ID SERIAL PRIMARY KEY, title text, description text, allday boolean, start timestamp, "end" timestamp, users uuid[], created timestamp)