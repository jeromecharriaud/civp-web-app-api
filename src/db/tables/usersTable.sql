DROP TABLE IF EXISTS users;

CREATE TABLE users (id uuid DEFAULT uuid_generate_v4 (), username VARCHAR(255) UNIQUE, name VARCHAR(255), email VARCHAR(100) UNIQUE, password VARCHAR(255),  avatar varchar(255), role VARCHAR(100), activated BOOLEAN, products_api_page INT, products_api_index INT, description text, CONSTRAINT users_info UNIQUE(uuid, username, email))