SELECT *
FROM information_schema.constraint_table_usage
WHERE table_name = 'likes_info';

DROP TABLE IF EXISTS likes;

CREATE TABLE likes (ID SERIAL PRIMARY KEY, user_id uuid, product_id bigint, liked boolean, CONSTRAINT likes_info UNIQUE(user_id, product_id))