DROP TABLE IF EXISTS products;

CREATE TABLE products (ID SERIAL PRIMARY KEY, title text, user_id uuid, description text, image varchar(255), created timestamp)