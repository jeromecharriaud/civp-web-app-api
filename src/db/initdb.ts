const db = require('../config/db')
const fs = require('fs')

const productsTable = fs.readFileSync('src/sql/productsTable.sql').toString()
const usersTable = fs.readFileSync('src/sql/usersTable.sql').toString()
const likesTable = fs.readFileSync('src/sql/likesTable.sql').toString()

//Create Users Table
const registerFunctionGenerateId = async () => {
	return await new Promise((resolve, reject) => {
		db.query(
			'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";',
			(err: any, res: any) => {
				resolve('success')
			}
		)
	})
}

//Create Users Table
const registerUsersTable = async () => {
	return await new Promise((resolve, reject) => {
		db.query(usersTable, (err: any, res: any) => {
			resolve(usersTable)
		})
	})
}

//Create Products Table
const createProductsTable = async () => {
	return await new Promise((resolve, reject) => {
		db.query(productsTable, (err: any, res: any) => {
			resolve(productsTable)
		})
	})
}

//Create Likes Table
const createLikesTable = async () => {
	return await new Promise((resolve, reject) => {
		db.query(likesTable, (err: any, res: any) => {
			resolve(likesTable)
		})
	})
}

const initDb = async () => {
	await Promise.all([
		await registerFunctionGenerateId(),
		await registerUsersTable(),
		await createProductsTable(),
		await createLikesTable(),
	])
		.then(() => {})
		.catch(() => {})
		.finally(() => {
			db.end()
		})
}

initDb()
