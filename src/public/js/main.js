//updating a product.
function updateProduct(productId) {
	//contact server
	return $.ajax({
		method: 'put',

		url: `/products/${productId}`,

		contentType: 'application/json',

		cache: false,

		error: (error) => {
			console.error(error)
		},
	})
}

//deleting a product.
function deleteProduct(productId) {
	//contact server
	return $.ajax({
		method: 'delete',

		url: `/products/${productId}`,

		contentType: 'application/json',

		cache: false,

		success: () => {
			location.reload()
		},

		error: (error) => {
			console.error(error)
		},
	})
}
