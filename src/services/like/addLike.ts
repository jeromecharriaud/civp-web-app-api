const db = require('../../config/db')
import LikeModel from '../../entities/Like.Entity'

/**
 * Get Like
 * @param params : Request parameters for querying like
 * @param params.liked : The Page of the Likes List
 * @param params.user_id : The Page of the Likes List
 * @param params.product_id : The Page of the Likes List
 *
 * @returns : The Like Object
 */
export const addLike = async function (
	like: LikeModel
): Promise<LikeModel[] | Error> {
	let { liked, user_id, product_id } = like
	if (liked === undefined) {
		liked = true
	}
	try {
		const result = await db.query(
			'INSERT INTO likes (liked, user_id, product_id) VALUES ($1, $2, $3) ON CONFLICT (user_id, product_id) DO UPDATE SET liked=NOT likes.liked RETURNING *',
			[liked, user_id, product_id]
		)
		const like = result.rows[0]
		return like
	} catch (error) {
		throw {
			code: 'like/cannot-add-like',
			message: "Can't add like",
			error: error,
		}
	}
}
module.exports = addLike
