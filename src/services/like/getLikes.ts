const db = require('../../config/db')
import LikeEntity from '../../entities/Like.Entity'

/**
 * Get Likes
 * @param params : Request parameters for querying likes
 * @param params.user_id : The Page of the Likes List
 * @param params.liked : The Page of the Likes List
 * @param params.product_id : The Page of the Likes List
 *
 * @returns : A list of Likes Objects
 */
export const getLikes = async function (params: {
	user_id: string
	product_id: string
	liked: boolean
}): Promise<LikeEntity[] | Error> {
	let query
	const { liked, user_id, product_id } = params
	let result: any = {
		results: {},
		rows: 0,
		total: 0,
	}

	try {
		if (params.product_id === undefined && params.user_id === undefined) {
			throw new Error('No request(s) entered !')
		}

		if (user_id !== undefined) {
			if (liked === undefined) {
				query = await db.query(
					`SELECT * FROM PRODUCTS INNER JOIN likes ON likes.product_id = products.id AND likes.user_id = $1`,
					[user_id]
				)
			} else {
				query = await db.query(
					`SELECT * FROM PRODUCTS INNER JOIN likes ON likes.product_id = products.id AND likes.user_id = $1 AND likes.liked = $2`,
					[user_id, liked]
				)
			}
		} else if (product_id !== undefined) {
			if (liked === undefined) {
				query = await db.query(
					`SELECT * FROM PRODUCTS INNER JOIN likes ON likes.product_id = products.id AND likes.product_id = $1`,
					[user_id]
				)
			} else {
				query = await db.query(
					`SELECT * FROM PRODUCTS INNER JOIN likes ON likes.product_id = products.id AND likes.product_id = $1 AND likes.liked = $2`,
					[product_id, liked]
				)
			}
		} else {
			return new Error('No request(s) enetered !')
		}
		if (query.rows) {
			result.results = query.rows
			result.rows = query.rowCount
			result.total = query.rows.length
		}
		return result
	} catch (e) {
		return new Error(e)
	}
}
module.exports = getLikes
