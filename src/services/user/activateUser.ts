const db = require('../../config/db')
import UserModel from '../../entities/User.Entity'

/**
 * Update a User
 * @param {UserModel} user_id : User Id you want to update
 * @returns {UserModel} : User Object updated
 */
export const activateUser = async function (
	user_id: UserModel['id']
): Promise<UserModel | Error> {
	try {
		const response = await db
			.query(
				`UPDATE users SET activated=NOT activated WHERE id=$1 RETURNING *`,
				[user_id]
			)
			.catch()
		if (response.rows[0]) {
			return response.rows[0]
		} else {
			throw Error("The User couldn't be activated")
		}
	} catch (e) {
		return e
	}
}
module.exports = activateUser
