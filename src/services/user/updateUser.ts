const db = require('../../config/db')
import UserModel from '../../entities/User.Entity'

/**
 * Update a User
 * @param {UserModel} user : User Object you want to update
 * @param {UserModel} data : Data of the user you want to change
 * @returns {UserModel} : User Object updated
 */
export const updateUser = async function (
	user: UserModel,
	data: UserModel
): Promise<UserModel | Error> {
	const user_id = user.id
	const newData = Object.assign(user, data)
	const {
		username,
		password,
		name,
		email,
		role,
		description,
		products_api_page,
		products_api_index,
	} = newData
	try {
		const response = await db.query(
			`UPDATE users SET name=$2, email=$3, role=$4, password=$5, description=$6, products_api_page=$7, products_api_index=$8 WHERE id=$1 RETURNING *`,
			[
				user_id,
				name,
				email,
				role,
				password,
				description,
				products_api_page,
				products_api_index,
			]
		)
		if (response.rows[0]) {
			return response.rows[0]
		} else {
			throw Error("The User couldn't be updated")
		}
	} catch (e) {
		return e
	}
}
module.exports = updateUser
