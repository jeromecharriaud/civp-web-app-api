const db = require('../../config/db')
import UserModel from '../../entities/User.Entity'

/**
 * Get User
 * @param params : Request parameters for querying user
 * @param params.user_id : The Page of the Users List
 * @param params.username : The Page of the Users List
 * @param params.email : The Page of the Users List
 *
 * @returns : The User Object
 */
const getUser = async (params: {
	user_id: string
	username: string
	email: string
}): Promise<UserModel[] | Error> => {
	let result
	try {
		//ID
		if (params.user_id) {
			result = await db.query(`SELECT * FROM users WHERE id=$1`, [
				params.user_id,
			])
		}
		//List of Users
		else if (params.username) {
			result = await db.query(`SELECT * FROM users WHERE username=$1`, [
				params.username,
			])
		}
		//Default Query
		else if (params.email) {
			result = await db.query(`SELECT * FROM users WHERE email=$1`, [
				params.email,
			])
		}
		const user = result.rows[0]
		return user
	} catch (e) {
		throw Error('Error Fetching User')
	}
}
module.exports = getUser
