const db = require('../../config/db')
import UserModel from '../../entities/User.Entity'

/**
 * Get Users
 * @param params : Request parameters for querying users
 * @param params.page : The Page of the Users List
 * @param params.limit : The Page of the Users List
 * @param params.offset : The Page of the Users List
 * @param params.users : Specific Users you want to get
 *
 * @returns : A list of Users Objects
 */
export const getUsers = async function (params: {
	page: number
	limit: number
	users: string[]
	offset: number
}): Promise<UserModel[] | Error> {
	//Preparing Query
	let result
	try {
		//Page
		if (params.page) {
			result = await db.query(`SELECT * FROM users LIMIT $1 OFFSET $2`, [
				params.limit,
				params.offset,
			])
		}
		//List of Users
		else if (params.users) {
			result = await db.query(`SELECT * FROM users WHERE id = ANY ($1)`, [
				params.users,
			])
		}
		//Default Query
		else {
			result = await db.query(`SELECT * FROM users`)
		}
		const users = result.rows
		//Remove Passwords
		users.forEach(function (v: any) {
			delete v.password
		})
		return users
	} catch (e) {
		throw Error('Error Fetching Users')
	}
}
module.exports = getUsers
