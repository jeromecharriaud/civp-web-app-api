const db = require('../../config/db')
import UserModel from '../../entities/User.Entity'

/**
 * Count Users
 * @param params : Request parameters for querying users
 * @param params.page : The Page of the Users List
 * @param params.limit : The Page of the Users List
 * @param params.offset : The Page of the Users List
 * @param params.users : Specific Users you want to get
 *
 * @returns : A list of Users Objects
 */
export const countUsers = async function (params: {
	users: string[]
}): Promise<UserModel[] | Error> {
	//Preparing Query
	let result

	//List of Users
	if (params.users) {
		result = await db.query(`SELECT * FROM users WHERE id = ANY ($1)`, [
			params.users,
		])
	}
	//Default Query
	else {
		result = await db.query(`SELECT COUNT(*) FROM users`)
	}
	try {
		const count = result.rows[0].count
		return count
	} catch (e) {
		throw Error('Error Fetching Users')
	}
}
module.exports = countUsers
