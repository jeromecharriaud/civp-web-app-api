const db = require('../../config/db')
const bcrypt = require('bcryptjs')
import UserModel from '../../entities/User.Entity'

/**
 * Get User
 * @param params : Request parameters for querying user
 * @param params.user_id : The Page of the Users List
 * @param params.username : The Page of the Users List
 * @param params.email : The Page of the Users List
 *
 * @returns : The User Object
 */

const addUser = async function (user: UserModel): Promise<UserModel[] | Error> {
	let { username, name, email, password, role } = user
	try {
		password = await bcrypt.hash(password, 8)
		const result = await db.query(
			'INSERT INTO users (username, name, email, password, role, activated, description, products_api_page, products_api_index) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *',
			[
				username.toLowerCase(),
				name,
				email,
				password,
				role,
				false,
				null,
				1,
				1,
			]
		)
		const user = result.rows[0]
		return user
	} catch (error) {
		let code
		let message
		if (error.constraint == 'users_username_key') {
			code = 'auth/username-taken'
			message =
				'This username has already been taken. Choose another one.'
		} else if (error.constraint == 'users_email_key') {
			code = 'auth/email-taken'
			message = 'This email has already been taken. Choose another one.'
		} else {
			code = 'auth/cannot-create-user'
			message = 'Your account cannot be created. Please try again later.'
		}
		return new Error(message)
	}
}
module.exports = addUser
