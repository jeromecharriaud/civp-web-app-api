const db = require('../../config/db')
import UserModel from '../../entities/User.Entity'

/**
 * Get User
 * @param params : Request parameters for querying user
 *
 * @returns : Boolean
 */
export const deleteUser = async function (
	user_id: UserModel['id']
): Promise<Boolean | Error> {
	try {
		const result = await db.query(`DELETE FROM users WHERE id=$1`, [
			user_id,
		])
		if (result.rowCount == 1) {
			return true
		} else {
			throw new Error('Error Deleting User')
		}
	} catch (e) {
		return e
	}
}
module.exports = deleteUser
