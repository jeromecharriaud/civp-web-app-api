const db = require('../../config/db')
import ProductModel from '../../entities/Product.Entity'

/**
 * Get Product
 * @param params : Request parameters for querying product
 * @param params.product_id : The Page of the Products List
 *
 * @returns : The Product Object
 */
const getProduct = async (
	product_id: string
): Promise<ProductModel[] | Error> => {
	try {
		let result = await db.query(`SELECT * FROM products WHERE id=$1`, [
			product_id,
		])
		const product = result.rows[0]
		return product
	} catch (e) {
		throw Error('Error Fetching Product')
	}
}
module.exports = getProduct
