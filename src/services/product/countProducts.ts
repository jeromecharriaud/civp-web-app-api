const db = require('../../config/db')
import ProductEntity from '../../entities/Product.Entity'

/**
 * Get Products
 * @param params : Request parameters for querying products
 * @param params.page : The Page
 * @param params.user_id : The Page of the Products List
 * @param params.products : The Page of the Products List
 * @param params.offset : The Page of the Products List
 *
 * @returns : A list of Products Objects
 */
export const countProducts = async function (params: {
	page: number
	user_id: string
	limit: number
	products: []
	offset: number
}): Promise<ProductEntity[] | Error> {
	//Preparing Query
	let where: Array<string> = ['1 = $1']
	let values: Array<string | number | []> = [1]
	let i = 2
	try {
		if (!params) {
			return new Error('No request(s) enetered !')
		}
		//User
		if (params.user_id) {
			where.push('AND user_id = $' + i)
			values.push(params.user_id)
			i++
		}
		//Products
		if (params.products) {
			let listProducts = params.products
			where.push('AND id = ANY $' + i)
			values.push(listProducts)
			i++
		}
		let request = {
			text: 'SELECT COUNT(*) FROM products WHERE ' + where.join(' '),
			values: values,
		}
		const query = await db.query(request)
		const count = query.rows.count
		return count
	} catch (e) {
		throw Error('Error Counting Products')
	}
}
module.exports = countProducts
