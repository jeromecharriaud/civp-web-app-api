const db = require('../../config/db')
import ProductModel from '../../entities/Product.Entity'

/**
 * Get Product
 * @param params : Request parameters for querying product
 *
 * @returns : Boolean
 */
export const deleteProduct = async function (
	product_id: ProductModel['id']
): Promise<Boolean | Error> {
	try {
		const result = await db.query(`DELETE FROM products WHERE id=$1`, [
			product_id,
		])
		if (result.rowCount == 1) {
			return true
		} else {
			throw new Error('Error Deleting Product')
		}
	} catch (e) {
		return e
	}
}
module.exports = deleteProduct
