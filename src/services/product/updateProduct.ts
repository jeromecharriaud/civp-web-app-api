const db = require('../../config/db')
import ProductModel from '../../entities/Product.Entity'

/**
 * Update a Product
 * @param {ProductModel} product : Product Object you want to update
 * @param {ProductModel} data : Data of the product you want to change
 * @returns {ProductModel} : Product Object updated
 */
export const updateProduct = async function (
	product: ProductModel,
	data: any
): Promise<ProductModel | Error> {
	const product_id = product.id
	const newData: ProductModel = { ...product, ...data }
	try {
		const response = await db
			.query(
				`UPDATE products SET title=$2, description=$3, image=$4 WHERE id=$1 RETURNING *`,
				[product_id, newData.title, newData.description, newData.image]
			)
			.catch()
		if (response.rows[0]) {
			return response.rows[0]
		} else {
			throw Error("The Product couldn't be updated")
		}
	} catch (e) {
		return e
	}
}
module.exports = updateProduct
