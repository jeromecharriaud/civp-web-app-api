const db = require('../../config/db')
import ProductModel from '../../entities/Product.Entity'

/**
 * Get Product
 * @param params : Request parameters for querying product
 * @param params.title : The Page of the Products List
 * @param params.description : The Page of the Products List
 * @param params.image : The Page of the Products List
 * @param params.user_id : The Page of the Products List
 *
 * @returns : The Product Object
 */
const addProduct = async function (
	product: ProductModel
): Promise<ProductModel[] | Error> {
	let { title, description, image, user_id } = product
	let created = new Date().toISOString()
	try {
		const result = await db.query(
			'INSERT INTO products (title, description, image, created, user_id) VALUES ($1, $2, $3, $4, $5) RETURNING *',
			[title, description, image, created, user_id]
		)
		const product = result.rows[0]
		return product
	} catch (error) {}
}
module.exports = addProduct
