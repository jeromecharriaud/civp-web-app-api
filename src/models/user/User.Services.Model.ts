export default interface UserServicesModel {
	addUser: Function
	countUsers: Function
	deleteUser: Function
	activateUser: Function
	getUser: Function
	getUsers: Function
	updateUser: Function
}
