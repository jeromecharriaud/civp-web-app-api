export default interface MiddlewaresModel {
	auth: Function
	roleAccess: Function
	checkEmail: Function
	userOnlyAccess: Function
}
