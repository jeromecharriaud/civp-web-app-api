import {
	UserControllerModel,
	UserControllerConstructor,
} from '../controllers/User.Controller'

import {
	ProductControllerModel,
	ProductControllerConstructor,
} from '../controllers/Product.Controller'

import {
	LikeControllerModel,
	LikeControllerConstructor,
} from '../controllers/Like.Controller'

export default interface ControllersModel {
	UserController(services: UserControllerConstructor): any
	ProductController(
		services: ProductControllerConstructor
	): ProductControllerModel
	LikeController(services: LikeControllerConstructor): LikeControllerModel
}
