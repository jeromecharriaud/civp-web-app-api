import { Response } from 'express'
import UserModel from '../entities/User.Entity'
export default interface CustomResponse extends Response {
	body: {
		credentials?: UserModel
		token: string
	}
}
