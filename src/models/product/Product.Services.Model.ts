export default interface ProductServicesModel {
	addProduct: Function
	countProducts: Function
	deleteProduct: Function
	getProduct: Function
	getProducts: Function
	updateProduct: Function
}
