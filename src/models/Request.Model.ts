import { Request } from 'express'
import UserModel from '../entities/User.Entity'
export default interface CustomRequest extends Request {
	user: UserModel
}
