/*
DROP TABLE IF EXISTS users;

CREATE TABLE users (id uuid DEFAULT uuid_generate_v4 (), username VARCHAR(255) UNIQUE, name VARCHAR(255), email VARCHAR(100) UNIQUE, password VARCHAR(255),  avatar varchar(255), role VARCHAR(100), activated BOOLEAN, products_api_page INT, products_api_index INT, description text, CONSTRAINT users_info UNIQUE(uuid, username, email))
*/

/**
 * User Model
 * @typedef {Object} UserModel
 * @property {string} id
 * @property {string} password
 * @property {string} email
 * @property {string} username
 * @property {string} avatar
 * @property {string} name
 * @property {string} role
 * @property {string} description
 * @property {boolean} activated
 * @property {number} products_api_page
 * @property {number} products_api_index
 */

interface UserModel {
	id: string
	password: string
	email: string
	username: string
	name: string
	avatar: string
	role: string
	description: string
	products_api_page: number
	products_api_index: number
}

export default class UserEntity implements UserModel {
	private _id?: string
	private _password?: string
	private _username: string
	private _name: string
	private _avatar: string
	private _email: string
	private _role: string
	private _description: string
	private _products_api_page: number
	private _products_api_index: number

	/** Get User Object
	 * @returns {Email, Username, Name, Role, Avatar, Description Products Api Page, Products Api Index} User Object
	 */
	get User(): {
		email: string
		username: string
		name: string
		role: string
		avatar: string
		description: string
		products_api_page: number
		products_api_index: number
	} {
		return {
			email: this.email,
			username: this.username,
			name: this.name,
			role: this.role,
			avatar: this.avatar,
			description: this.description,
			products_api_page: this.products_api_page,
			products_api_index: this.products_api_index,
		}
	}

	/**
	 * Get User ID
	 * @returns {string} User ID
	 */
	get id(): string {
		return this._id
	}

	/**
	 * Get Email
	 * @returns {string} User Email
	 */
	get email(): string {
		return this._email
	}

	/**
	 * Get Password
	 * @returns {string} User Email
	 */
	get password(): string {
		return this._password
	}

	/**
	 * Get Username
	 * @returns {string} Username
	 */
	get username(): string {
		return this._username
	}

	/**
	 * Get Avatar
	 * @returns {string} Avatar
	 */
	get avatar(): string {
		return this._avatar
	}

	/**
	 * Get Name
	 * @returns {string} Name
	 */
	get name(): string {
		return this._name
	}

	/**
	 * Get Role
	 * @returns {string} Role
	 */
	get role(): string {
		return this._role
	}
	/**
	 * Get Description
	 * @returns {string} Description
	 */
	get description(): string {
		return this._description
	}

	/**
	 * Get Products Api Page
	 * @returns {number} Role
	 */
	get products_api_page(): number {
		return this._products_api_page
	}

	/**
	 * Get Products Api Index
	 * @returns {number} Role
	 */
	get products_api_index(): number {
		return this._products_api_index
	}
}
