/**
 * Product Model
 * @typedef {Object} ProductModel
 * @property {int} id
 * @property {user_id} user_id
 * @property {string} title
 * @property {string} description
 * @property {string} created
 * @property {string} image
 */
interface ProductModel {
	id: number
	title: string
	description: string
	created: string
	image: string
	user_id: string
}
export default class ProductEntity implements ProductModel {
	private _id: number
	private _user_id: string
	private _title: string
	private _description: string
	private _created: string
	private _image: string

	/**
	 * Get Product
	 * @returns {ProductModel} Product
	 */
	get Product(): ProductModel {
		return this
	}

	/**
	 * Get Product Id
	 * @returns {number} Product Id
	 */
	get id(): number {
		return this._id
	}

	/**
	 * Get User Id
	 * @returns {string} User Id
	 */
	get user_id(): string {
		return this._user_id
	}

	/**
	 * Get Title
	 * @returns {string} Title
	 */
	get title(): string {
		return this._title
	}

	/**
	 * Get Description
	 * @returns {string} Description
	 */
	get description(): string {
		return this._description
	}

	/**
	 * Get Created Date
	 * @returns {string} Created
	 */
	get created(): string {
		return this._created
	}

	/**
	 * Get Image
	 * @returns {string} Image
	 */
	get image(): string {
		return this._image
	}
}
