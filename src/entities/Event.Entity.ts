/**
 * Event Model
 * @typedef {Object} EventModel
 * @property {int} id
 * @property {string} title
 * @property {string} description
 * @property {boolean} allday
 * @property {string} start
 * @property {string} end
 * @property {array} users
 */
export default class EventModel {
	private _id: number
	private _title: string
	private _description: string
	private _allday: boolean
	private _image: string
	private _users: string

	/**
	 * Get Event
	 * @returns {EventModel} Event
	 */
	get Event(): EventModel {
		return this
	}

	/**
	 * Get Id
	 * @returns {number} Id
	 */
	get id(): number {
		return this._id
	}

	/**
	 * Get Users
	 * @returns {string} Users
	 */
	get users(): string {
		return this._users
	}

	/**
	 * Get Title
	 * @returns {string} Title
	 */
	get title(): string {
		return this._title
	}

	/**
	 * Get Description
	 * @returns {string} Description
	 */
	get description(): string {
		return this._description
	}

	/**
	 * Get All Day
	 * @returns {boolean} All Day
	 */
	get allday(): boolean {
		return this._allday
	}

	/**
	 * Get Image
	 * @returns {string} Image
	 */
	get image(): string {
		return this._image
	}
}
