/**
 * Like Model
 * @typedef {Object} LikeModel
 * @property {int} id
 * @property {string} user_id
 * @property {number} like_id
 * @property {boolean} liked
 */
interface LikeModel {
	id: number
	user_id: string
	like_id: number
	product_id: number
	liked: boolean
}
export default class LikeEntity implements LikeModel {
	private _id: string
	private _user_id: string
	private _like_id: number
	private _product_id: number
	private _liked: boolean

	/**
	 * Get Like
	 * @returns {LikeModel} Like
	 */
	get Like(): LikeModel {
		return this
	}

	/**
	 * Get User ID
	 * @returns {strnumbering} User ID
	 */
	get id(): number {
		return this.id
	}

	/**
	 * Get User ID
	 * @returns {string} User ID
	 */
	get user_id(): string {
		return this._user_id
	}

	/**
	 * Get User ID
	 * @returns {number} User ID
	 */
	get product_id(): number {
		return this._product_id
	}

	/**
	 * Get Like
	 * @returns {number} Like
	 */
	get like_id(): number {
		return this._like_id
	}

	/**
	 * Get Like
	 * @returns {boolean} Like
	 */
	get liked(): boolean {
		return this._liked
	}
}
