import { Router } from 'express'
const express = require('express')
const router = express.Router()
//Middlewares
const middlewares = {
	auth: require('../middleware/auth'),
	roleAccess: require('../middleware/roleAccess'),
	checkEmail: require('../middleware/checkEmail'),
	userOnlyAccess: require('../middleware/userOnlyAccess'),
}

//Routes - Users
const UserController = require('../controllers/User.Controller')
const usersRoute = require('./User.Route.ts')
router.use(
	'/api/users',
	usersRoute({ UserController: UserController }, middlewares)
)

//Routes - Products
const ProductController = require('../controllers/Product.Controller')
const productsRoute = require('./Product.Route.ts')
router.use(
	'/api/products',
	productsRoute(
		{
			UserController: UserController,
			ProductController: ProductController,
		},
		middlewares
	)
)

//Routes - Likes
const LikeController = require('../controllers/Like.Controller')
const likesRoute = require('./Like.Route')
router.use(
	'/api/likes',
	likesRoute(
		{
			UserController: UserController,
			LikeController: LikeController,
		},
		middlewares
	)
)

//Routes - Events
// const EventController = require('../controllers/Event.Controller')
// const eventsRoute = require('./Event.Route')
// router.use('/api/events', eventsRoute(UserController, EventController))

//Routes - Emails
// const EmailController = require('../controllers/Email.Controller')
// const emailsRoute = require('./Email.Route')
// router.use('/api/emails', emailsRoute(UserController, EmailController))

//Routes - Files
// const FileController = require('../controllers/File.Controller')
// const filesRoute = require('./File.Route')
// router.use('/api/files', filesRoute(UserController, FileController))

//Homepage
// router.get('/', async (req: any, res: any, next: any) => {
// 	res.redirect('/login')
// })
module.exports = router
