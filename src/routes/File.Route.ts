export {}
import { unlink } from 'fs'
function declareRoutes() {
	const auth = require('../middleware/auth')
	const roleAccess = require('../middleware/roleAccess')
	const express = require('express')
	const router = express.Router()
	const routeUpload = '/api/upload'

	const fs = require('fs')
	const paths = require('../paths.ts')
	const model = 'file'
	router.post(routeUpload, [
		auth,
		roleAccess(model, 'read'),
		async (req: any, res: any, next: any) => {
			if (!req.files) {
				return res.status(500).send({ msg: 'file is not found' })
			}
			// accessing the file
			const myFile = req.files.file

			if (!myFile) {
				return res.status(500).send({ msg: 'file is not found' })
			}
			let path: string
			let checkPath: string
			let name: string
			let fileName = myFile.name.split('.').slice(0, -1).join('.')
			const extension = myFile.name.split('.')[1]

			if (req.body && req.body.route && req.body.route != 'undefined') {
				const route = req.body.route
				path = `${paths.images}/${route}/`
				checkPath = `${paths.images}/${route}/${myFile.name}`
			} else {
				path = `${paths.images}/`
				checkPath = `${paths.images}/${myFile.name}`
			}

			//Rename the file if exists
			fs.access(checkPath, fs.F_OK, (err: any) => {
				if (!err) {
					fileName += '-' + generateString(12)
				}
				name = fileName + '.' + extension
				path += name

				//  mv() method places the file inside public directory
				myFile.mv(path, function (err: any) {
					if (err) {
						return res.status(500).send({ msg: 'Error occured' })
					}
					return res.send({
						message: 'File updated!',
						name: name,
						path: path,
					})
				})
			})
		},
	])
	router.delete(routeUpload, [
		auth,
		roleAccess(model, 'delete'),
		async (req: any, res: any, next: any) => {
			let path = ''
			const name = req.body.name
			const route = req.body.route

			if (req.body && req.body.route && req.body.route != 'undefined') {
				const route = req.body.route
				path = `${paths.images}/${route}/`
			} else {
				path = `${paths.images}/`
			}

			path += name
			unlink(path, (err) => {
				if (err) {
					return res.status(500).send({ msg: 'Error occured' })
				}
				return res.send({
					message: 'File deleted !',
					name: name,
					path: path,
				})
			})
		},
	])
	const characters =
		'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
	function generateString(length: number) {
		let result = ''
		const charactersLength = characters.length
		for (let i = 0; i < length; i++) {
			result += characters.charAt(
				Math.floor(Math.random() * charactersLength)
			)
		}

		return result
	}
	return router
}

module.exports.declareRoutes = declareRoutes
