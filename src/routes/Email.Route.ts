export {}
function declareRoutes() {
	const auth = require('../middleware/auth')
	const roleAccess = require('../middleware/roleAccess')
	const express = require('express')
	const router = express.Router()
	const routeEmail = '/api/email'
	const model = 'email'

	router.post(routeEmail + '/send', [
		async (req: any, res: any, next: any) => {
			const { name, email, emailSubject, emailContent } = req.body

			// Include the Sendinblue library\
			var SibApiV3Sdk = require('sib-api-v3-sdk')
			var defaultClient = SibApiV3Sdk.ApiClient.instance
			// Instantiate the client\
			var apiKey = defaultClient.authentications['api-key']
			apiKey.apiKey = process.env.EMAIL_API_KEY

			var apiInstance = new SibApiV3Sdk.TransactionalEmailsApi()
			var sendSmtpEmail = new SibApiV3Sdk.SendSmtpEmail()

			//Define the campaign settings
			sendSmtpEmail = {
				to: [
					{
						email: email,
						name: name,
					},
				],
				params: {
					name: process.env.NAME,
				},
				headers: {},
				subject: emailSubject,
				htmlContent: emailContent,
			}

			apiInstance.sendTransacEmail(sendSmtpEmail).then(
				function (data: any) {
					return res.status(200).send({
						status: 200,
						message: 'Email sent successfully',
						content: data,
					})
				},
				function (error: any) {
					return res.status(501).send({
						status: 501,
						message: 'Error sending the email',
						content: error,
					})
				}
			)
		},
	])
	return router
}
module.exports.declareRoutes = declareRoutes
