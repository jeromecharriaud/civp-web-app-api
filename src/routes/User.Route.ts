//Models
import { Request, Response, NextFunction } from 'express'
import MiddlewaresModel from '../models/Middlewares.Model'
import ControllersModel from '../models/Controllers.Model'
import CustomRequest from '../models/Request.Model'
const UserRoutes = (
	Controllers: ControllersModel,
	Middlewares: MiddlewaresModel
) => {
	const express = require('express')
	const router = express.Router()

	//Controllers
	const UserController = <any>Controllers.UserController

	//Middlewares
	const auth = Middlewares.auth
	const roleAccess = Middlewares.roleAccess
	const checkEmail = Middlewares.checkEmail
	const userOnlyAccess = Middlewares.userOnlyAccess

	//Services
	const getUser = require('../services/user/getUser')
	const getUsers = require('../services/user/getUsers')
	const activateUser = require('../services/user/activateUser')
	const addUser = require('../services/user/addUser')
	const updateUser = require('../services/user/updateUser')
	const deleteUser = require('../services/user/deleteUser')
	const countUsers = require('../services/user/countUsers')

	//Capacities
	const capabilities = require('../roles')

	const jwt = require('jsonwebtoken')
	const model = 'user'
	const rateLimit = require('express-rate-limit')
	const apiLimiter = rateLimit({
		windowMs: 1 * 60 * 1000, // 15 minutes
		max: 5, // Limit each IP to 5 requests per `window` (here, per 15 minutes)
		standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
		legacyHeaders: false, // Disable the `X-RateLimit-*` headers
	})

	//Login
	router.post(
		'/user/login',
		apiLimiter,
		async (req: Request, res: Response, next: NextFunction) => {
			try {
				const User = new UserController({
					getUser: getUser,
				})
				const credentials = await User.loginUser(
					req.body.email,
					req.body.password
				)
				const result = await User.generateAuthToken(credentials)
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				// req.session.token = token
				res.status(200).send({
					status: 200,
					message: 'Token Created',
					credentials: credentials,
					token: result,
				})
				// res.redirect('/')
			} catch (e) {
				next(e)
				// res.render('login', { error: e })
			}
		}
	)

	//Logout
	router.post(
		'/user/logout',
		async (req: Request, res: Response, next: NextFunction) => {
			try {
				res.redirect('/')
			} catch (e) {
				next(e)
			}
		}
	)

	//Check Token
	router.post(
		'/user/check-token',
		apiLimiter,
		async (req: Request, res: Response, next: NextFunction) => {
			const token = req.header('Authorization').replace('Bearer ', '')
			try {
				const result = jwt.verify(token, process.env.TOKEN_KEY)
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				res.status(200).send({
					status: 200,
					message: 'Token Verified !',
					checked: true,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Generate Token
	router.post(
		'/user/generate-token',
		apiLimiter,
		async (req: Request, res: Response, next: NextFunction) => {
			try {
				if (!req.body.user_id) {
					throw {
						code: 'user/no-user-id',
						message: 'Requires an user id',
						error: new Error(),
					}
				}
				const User = new UserController({
					getUser: getUser,
				})
				const user_id = req.body.user_id
				const token = await User.generateToken(user_id)

				const result = await User.getUser({
					user_id: user_id,
				})
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				res.status(200).send({
					status: 200,
					message: 'Token Created',
					token: token,
					credentials: result,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Lost Password
	router.post(
		'/user/lost-password',
		apiLimiter,
		checkEmail(Controllers.UserController),
		async (req: CustomRequest, res: Response, next: NextFunction) => {
			try {
				const User = new UserController({
					getUser: getUser,
				})
				const credentials = req.user
				const result = await User.generateToken(req.user.id)
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				res.status(200).send({
					status: 200,
					message: 'Lost Password Token Created',
					token: result,
					credentials: credentials,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Check Token - Lost Passowrd
	router.post(
		'/user/lost-password-check-token',
		apiLimiter,
		async (req: Request, res: Response, next: NextFunction) => {
			const { token } = req.body
			try {
				const decoded = jwt.verify(token, process.env.TOKEN_KEY)
				if (decoded instanceof Error) {
					throw new Error(decoded.message)
				}
				res.send({
					status: 200,
					message: 'Lost Password Token Verified !',
					checked: true,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Get users.
	router.get(
		'',
		auth(Controllers.UserController),
		roleAccess(model, 'read'),
		async (req: Request, res: Response, next: NextFunction) => {
			let params = {}
			if (req.query) {
				params = req.query
			}
			try {
				const User = new UserController({
					getUsers: getUsers,
					countUsers: countUsers,
				})
				const result = await User.getUsers(params)
				//List of users
				if (result.total >= 1) {
					res.status(200).send({
						status: 200,
						message: 'Users Fetched !',
						elementsPerPage:
							result.page !== undefined
								? parseInt(process.env.NB_ELEMENTS_PER_PAGE)
								: '',
						page: result.page !== undefined ? result.page : '',
						offset:
							result.offset !== undefined ? result.offset : '',
						rows: result.rows,
						total: parseInt(result.total),
						result: result.results,
					})
				} else {
					throw {
						code: 'user/no-users',
						message: 'No Users Found',
						error: new Error(),
					}
				}
			} catch (e) {
				next(e)
			}
		}
	)

	//Get a user by ID
	router.get(
		'/user/id/:user_id',
		auth(Controllers.UserController),
		userOnlyAccess(Controllers.UserController),
		roleAccess(model, 'read'),
		async (req: Request, res: Response, next: NextFunction) => {
			let { user_id } = req.params
			try {
				const User = new UserController({
					getUser: getUser,
				})
				const result = await User.getUser({
					user_id: user_id,
				})
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				res.status(200).send({
					status: 200,
					message: 'User Fetched !',
					credentials: result,
					capabilities: capabilities[result.role],
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Get a user by Email
	router.get(
		'/user/email/:email',
		auth(Controllers.UserController),
		roleAccess(model, 'read'),
		async (req: Request, res: Response, next: NextFunction) => {
			let { email } = req.params
			try {
				const User = new UserController({
					getUser: getUser,
				})
				const result = await User.getUser({
					email: email,
				})
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				res.status(200).send({
					status: 200,
					message: 'User Fetched !',
					credentials: result,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Get a user by Username
	router.get(
		'/user/username/:username',
		auth(Controllers.UserController),
		roleAccess(model, 'read'),
		async (req: Request, res: Response, next: NextFunction) => {
			let { username } = req.params
			try {
				const User = new UserController({
					getUser: getUser,
				})
				const result = await User.getUser({
					username: username,
				})
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				res.status(200).send({
					status: 200,
					message: 'User Fetched !',
					credentials: result,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Get a user password
	router.get(
		'/user/:user_id/password',
		auth(Controllers.UserController),
		userOnlyAccess(Controllers.UserController),
		roleAccess(model, 'read'),
		async (req: Request, res: Response, next: NextFunction) => {
			let { user_id } = req.params
			try {
				const User = new UserController({
					getUser: getUser,
				})
				const result = await User.getUserPassword(user_id)
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				res.status(200).send({
					status: 200,
					message: 'User Password Fetched !',
					password: result,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Create a user.
	router.post(
		'/user/add',
		apiLimiter,
		async (req: Request, res: Response, next: NextFunction) => {
			const user = req.body
			try {
				const User = new UserController({
					addUser: addUser,
				})
				const result = await User.registerUser(user)
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				res.status(200).send({
					status: 200,
					message: 'User created',
					credentials: result,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Update a user.
	router.put(
		'/user/update',
		auth(Controllers.UserController),
		userOnlyAccess(Controllers.UserController),
		roleAccess(model, 'update'),
		async (req: CustomRequest, res: Response, next: NextFunction) => {
			const data = req.body
			try {
				let user_id
				let email
				if (data.user_id !== undefined) {
					user_id = data.user_id
				} else {
					user_id = req.user.id
				}
				const User = new UserController({
					getUser: getUser,
					updateUser: updateUser,
				})
				const user = await User.getUser({
					user_id: user_id,
				})
				if (!user) {
					throw new Error('No user Found')
				}
				const result = await User.updateUser(user, data)
				delete result.password
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				res.status(200).send({
					status: 200,
					message: 'User updated !',
					credentials: result,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Activate a user.
	router.put(
		'/user/activate',
		apiLimiter,
		async (req: Request, res: Response, next: NextFunction) => {
			try {
				const { token } = req.body
				const decoded = await jwt.verify(token, process.env.TOKEN_KEY)
				const User = new UserController({
					activateUser: activateUser,
				})
				const response = await User.activateUser(decoded._id)
				delete response.password
				if (response instanceof Error) {
					throw new Error(response.message)
				}
				res.status(200).send({
					status: 200,
					message: 'User Activated !',
					credentials: response,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Update a user password
	router.put(
		'/user/update-password',
		auth(Controllers.UserController),
		userOnlyAccess(Controllers.UserController),
		async (req: Request, res: Response, next: NextFunction) => {
			try {
				const { password, email } = req.body
				const token = req.header('Authorization').replace('Bearer ', '')
				const decoded = await jwt.verify(token, process.env.TOKEN_KEY)
				const user_id = decoded._id
				if (!password) {
					throw {
						code: 'user/password-missing',
						message: 'Password is missing',
						error: new Error(),
					}
				}
				const User = new UserController({
					getUser: getUser,
					updateUser: updateUser,
				})
				const user = await User.getUser({
					email: email,
					user_id: user_id,
				})
				if (!user) {
					throw {
						code: 'user/no-user',
						message: 'No User has been found',
						error: new Error(),
					}
				}
				const result = await User.updateUserPassword(user, password)
				delete result.password
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				res.status(200).send({
					status: 200,
					message: 'Password updated !',
					credentials: result,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Delete a user.
	router.delete(
		'/user/:user_id',
		auth(Controllers.UserController),
		userOnlyAccess(Controllers.UserController),
		roleAccess(model, 'delete'),
		async (req: Request, res: Response, next: NextFunction) => {
			try {
				let { user_id } = req.params
				const User = new UserController({
					deleteUser: deleteUser,
				})
				let result = await User.deleteUser(user_id)
				if (result instanceof Error) {
					throw new Error('Error deleting the user')
				} else {
					res.status(200).send({
						message: 'User deleted !',
					})
				}
			} catch (e) {
				next(e)
			}
		}
	)
	return router
}

module.exports = UserRoutes
