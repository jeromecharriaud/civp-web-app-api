import { Request, Response, NextFunction } from 'express'
import ControllersModel from '../models/Controllers.Model'
import MiddlewaresModel from '../models/Middlewares.Model'
import CustomRequest from '../models/Request.Model'
const ProductRoutes = (
	Controllers: ControllersModel,
	Middlewares: MiddlewaresModel
) => {
	const express = require('express')
	const model = 'product'
	const router = express.Router()

	//Middlewares
	const auth = Middlewares.auth
	const roleAccess = Middlewares.roleAccess

	//Controllers
	const ProductController = <any>Controllers.ProductController
	const UserController = <any>Controllers.UserController

	//Services
	const getProduct = require('../services/product/getProduct')
	const getProducts = require('../services/product/getProducts')
	const addProduct = require('../services/product/addProduct')
	const updateProduct = require('../services/product/updateProduct')
	const deleteProduct = require('../services/product/deleteProduct')
	const countProducts = require('../services/product/countProducts')

	//Get Products
	router.get('/', [
		auth(UserController),
		roleAccess(model, 'read'),
		async (req: Request, res: Response, next: NextFunction) => {
			let params = {}
			if (req.query) {
				params = req.query
			}
			try {
				const Controller = new ProductController({
					getProducts: getProducts,
					countProducts: countProducts,
				})
				const result = await Controller.getProducts(params)
				if (result instanceof Error) {
					throw new Error(result.message)
				} else {
					res.status(200).send({
						status: 200,
						message: 'Products Fetched !',
						page: result.page !== undefined ? result.page : '',
						elementsPerPage:
							result.page !== undefined
								? parseInt(process.env.NB_ELEMENTS_PER_PAGE)
								: '',
						offset:
							result.offset !== undefined ? result.offset : '',
						rows: result.rows,
						total: result.total,
						result: result.results,
					})
				}
			} catch (e) {
				next(e)
			}
		},
	])

	//Get a product
	router.get(
		'/product/:productId',
		auth(UserController),
		roleAccess(model, 'read'),
		async (req: Request, res: Response, next: NextFunction) => {
			let { productId } = req.params
			try {
				const Controller = new ProductController({
					getProduct: getProduct,
				})
				const result = await Controller.getProduct(productId)
				if (result instanceof Error) {
					throw new Error(result.message)
				} else {
					res.status(200).send({
						status: 200,
						message: 'Product fetched!',
						result: result,
					})
				}
			} catch (e) {
				next(e)
			}
		}
	)

	//Create a product.
	router.post(
		'/add',
		auth(UserController),
		roleAccess(model, 'create'),
		async (req: CustomRequest, res: Response, next: NextFunction) => {
			const product = { ...req.body, ...{ user_id: req.user.id } }
			try {
				const Controller = new ProductController({
					addProduct: addProduct,
				})
				const result = await Controller.createProduct(product)

				if (result instanceof Error) {
					throw new Error(result.message)
				} else {
					res.status(200).send({
						status: 200,
						message: 'Product created',
						result: result,
					})
				}
			} catch (e) {
				next(e)
			}
		}
	)

	//Update a product.
	router.put(
		'/:product_id',
		auth(UserController),
		roleAccess(model, 'update'),
		async (req: Request, res: Response, next: NextFunction) => {
			const { product_id } = req.params
			const data = req.body
			try {
				const Controller = new ProductController({
					getProduct: getProduct,
					updateProduct: updateProduct,
				})
				const product = await Controller.getProduct(product_id)
				const newProduct = await Controller.updateProduct(product, data)
				if (newProduct instanceof Error) {
					throw new Error(newProduct.message)
				} else {
					res.status(200).send(newProduct)
				}
			} catch (e) {
				next(e)
			}
		}
	)

	//Delete a product.
	router.delete(
		'/:productId',
		auth(UserController),
		roleAccess(model, 'delete'),
		async (req: Request, res: Response, next: NextFunction) => {
			let { productId } = req.params
			try {
				const Controller = new ProductController({
					getProduct: getProduct,
					deleteProduct: deleteProduct,
				})
				const product = await Controller.getProduct(productId)
				let result = await Controller.deleteProduct(productId)
				if (result instanceof Error) {
					throw new Error(result.message)
				} else {
					res.status(200).send({
						message: 'Product deleted !',
					})
				}
			} catch (e) {
				next(e)
			}
		}
	)
	return router
}
module.exports = ProductRoutes
