export {}
function declareRoutes(Controller: any) {
	const auth = require('../middleware/auth')
	const roleAccess = require('../middleware/roleAccess')
	const express = require('express')
	const router = express.Router()
	const Event = require('../controllers/event')
	const routeEvents = '/api/events/'

	const model = 'event'
	//Homepage
	router.get('/', async (req: any, res: any) => {
		let events = await new Controller().getEvents()
		return res.render('home', {
			events,
		})
	})

	//Get all events.
	router.get(routeEvents, [
		auth,
		roleAccess(model, 'read'),
		async (req: any, res: any, next: any) => {
			let params = {}
			if (req.query) {
				params = req.query
			}
			try {
				const result = await new Controller().getEvents(params)
				res.status(200).send({
					status: 200,
					message: 'Events Fetched !',
					page: result.page !== undefined ? result.page : '',
					elementsPerPage:
						result.page !== undefined
							? parseInt(process.env.NB_ELEMENTS_PER_PAGE)
							: '',
					offset: result.offset !== undefined ? result.offset : '',
					rows: result.rows,
					total: result.total,
					result: result.results,
				})
			} catch (e) {
				next(e)
			}
		},
	])
	//Get a event
	roleAccess(model, 'read', 'administrator'),
		router.get(
			routeEvents + 'event/:eventId',
			auth,
			roleAccess(model, 'read'),
			async (req: any, res: any, next: any) => {
				let { eventId } = req.params
				try {
					const event = await new Controller().getEvent(eventId)
					if (!event) {
						throw new Error('No event')
					}
					res.status(200).send(event)
				} catch (e) {
					res.status(500).send({
						status: 500,
						message: e.message,
					})
				}
			}
		)

	//Create a event.
	router.post(
		routeEvents + 'add',
		auth,
		roleAccess(model, 'create'),
		async (req: any, res: any, next: any) => {
			const event = req.body
			try {
				const newEvent = await new Controller().createEvent(event)
				res.status(200).send({
					status: 200,
					message: 'Event created',
					result: newEvent,
				})
			} catch (e) {
				res.status(500).send({
					status: 500,
					message: e.message,
				})
			}
		}
	)

	//Update a event.
	router.put(
		routeEvents + ':eventId',
		auth,
		roleAccess(model, 'update'),
		async (req: any, res: any, next: any) => {
			const { eventId } = req.params
			const data = req.body
			try {
				const event = await new Controller().getEvent(eventId)
				const newEvent = await new Controller().updateEvent(event, data)

				if (!event) {
					throw new Error('No event')
				}
				res.status(200).send(newEvent)
			} catch (e) {
				res.status(500).send({
					status: 500,
					message: e.message,
				})
			}
		}
	)

	/** Event : Register a user
	 * @param {uuid} user The User being registered
	 * @returns {object} The event
	 */
	router.put(
		routeEvents + ':event_id/register',
		auth,
		roleAccess(model, 'read'),
		async (req: any, res: any, next: any) => {
			const { event_id } = req.params
			const { user_id } = req.body
			try {
				const event = await new Controller().registerUser({
					event_id: event_id,
					user_id: user_id,
				})
				if (!event) {
					throw new Error('Not able to register the user !')
				}
				res.status(200).send(event)
			} catch (e) {
				res.status(500).send({
					status: 500,
					message: e.message,
				})
			}
		}
	)

	//Delete a event.
	router.delete(
		routeEvents + ':event_id',
		auth,
		roleAccess(model, 'delete'),
		async (req: any, res: any, next: any) => {
			let { event_id } = req.params
			try {
				const event = await new Controller().getEvent(event_id)
				let result = await new Controller().deleteEvent(event_id)
				if (!result) {
					throw new Error('Deletion failed')
				}
				res.status(200).send(event)
			} catch (e) {
				res.status(500).send({
					status: 500,
					message: e.message,
				})
			}
		}
	)
	return router
}

module.exports.declareRoutes = declareRoutes
