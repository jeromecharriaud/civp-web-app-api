const express = require('express')
const router = express.Router()
//Models
import { Request, Response, NextFunction } from 'express'
import MiddlewaresModel from '../models/Middlewares.Model'
import ControllersModel from '../models/Controllers.Model'
import CustomRequest from '../models/Request.Model'

//Services
const getLikes = require('../services/like/getLikes')
const addLike = require('../services/like/addLike')

const LikeRoutes = (
	Controllers: ControllersModel,
	Middlewares: MiddlewaresModel
) => {
	//Middlewares
	const auth = Middlewares.auth
	const roleAccess = Middlewares.roleAccess

	//Controllers
	const LikeController = <any>Controllers.LikeController
	const UserController = <any>Controllers.UserController

	const model = 'like'

	//Get Likes
	router.get(
		'',
		auth(UserController),
		roleAccess(model, 'read'),
		async (req: CustomRequest, res: Response, next: NextFunction) => {
			try {
				let { product_id, liked, by } = req.query
				let params = {
					product_id: product_id,
					liked: liked,
				}
				if (by === 'user') {
					params = { ...params, ...{ user_id: req.user.id } }
				}
				const Controller = new LikeController({
					getLikes: getLikes,
				})
				const result = await Controller.getLikes(params)
				if (result instanceof Error) {
					throw new Error(result.message)
				}
				res.status(200).send({
					status: 200,
					message: 'Likes Fetched !',
					rows: result.rows,
					total: result.total,
					result: result.results,
				})
			} catch (e) {
				next(e)
			}
		}
	)

	//Create Like
	router.post(
		'/add',
		auth(UserController),
		roleAccess(model, 'create'),
		async (req: any, res: any, next: any) => {
			const like = req.body
			try {
				const Controller = new LikeController({
					addLike: addLike,
				})
				const newLike = await Controller.createLike(like)
				res.status(200).send({
					status: 200,
					message: 'Product liked/unliked',
					result: newLike,
				})
			} catch (e) {
				next(e)
			}
		}
	)
	return router
}
module.exports = LikeRoutes
