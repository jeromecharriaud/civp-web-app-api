import CustomResponse from '../../../src/models/Response.Model'
const express = require('express')
const supertest = require('supertest')

const app = new express()
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

const route = '/api/users'

//Routes
app.use(require('../../../src/routes'))

app.use(
	express.urlencoded({
		extended: false,
	})
)

describe('User Registration Scenario', function () {
	//Non logged in user tried to access to forbbiden data
	test('Should not be able to access to protected data', (done) => {
		supertest(app)
			.get(route)
			.set('Accept', 'application/json')
			.expect(500)
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})

	//User registers and gets an ID
	test('Register User', (done) => {
		supertest(app)
			.post(route + '/user/add')
			.type('form')
			.send({
				email: process.env.TEST_TEMP_USER_EMAIL,
				password: process.env.TEST_TEMP_USER_PASSWORD,
				username: process.env.TEST_TEMP_USER_USERNAME,
				name: process.env.TEST_TEMP_USER_NAME,
				role: process.env.TEST_TEMP_USER_ROLE,
			})
			.set('Accept', 'application/json')
			.expect(200)
			.expect(function (res: CustomResponse) {
				res.body.credentials.id.length > 0
			})
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})
	//Login and register Token
	test('The User Logs in', (done) => {
		supertest(app)
			.post(route + '/user/login')
			.type('form')
			.send({
				email: process.env.TEST_TEMP_USER_EMAIL,
				password: process.env.TEST_TEMP_USER_PASSWORD,
			})
			.set('Accept', 'application/json')
			.expect(200)
			.expect(function (res: Response | any) {
				res.body.token.length > 0
				res.body.credentials.id.length > 0
				app.set('token', res.body.token)
				app.set('user_id', res.body.credentials.id)
			})
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})

	//Check Token
	test("Let's check the token of the user newly created", (done) => {
		supertest(app)
			.post(route + '/user/check-token')
			.type('form')
			.set('Accept', 'application/json')
			.set('Authorization', app.get('token'))
			.expect(200)
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})

	//User Access to Protected Data
	test('Should be able to access to protected data : Liste of Users', (done) => {
		supertest(app)
			.get(route)
			.set('Accept', 'application/json')
			.set('Authorization', app.get('token'))
			.expect(200)
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})

	//User Access to its own Data
	test('Access to his own user data', (done) => {
		supertest(app)
			.get(route + '/user/id/' + app.get('user_id'))
			.set('Accept', 'application/json')
			.set('Authorization', app.get('token'))
			.expect(200)
			.expect(function (res: Response | any) {
				res.body.credentials.id === app.get('user_id')
			})
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})

	//User Updates its account details
	test('Updating own information', (done) => {
		supertest(app)
			.put(route + '/user/update')
			.type('form')
			.set('Authorization', app.get('token'))
			.send({
				name: 'MyNewName',
			})
			.set('Accept', 'application/json')
			.expect(200)
			.expect(function (res: Response | any) {
				res.body.credentials.name === 'MyNewName'
			})
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})
	//User deletes its account
	test('User deletes its account', (done) => {
		supertest(app)
			.delete(route + '/user/' + app.get('user_id'))
			.type('form')
			.set('Authorization', app.get('token'))
			.expect(200)
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})
})
