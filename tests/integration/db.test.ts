export {}
const db = require('../../src/config/db')

describe('Testing Database Connection', () => {
	let pgPool: any

	beforeAll(() => {
		pgPool = db
	})

	afterAll(async () => {
		await pgPool.end()
	})

	it('Postgres Connection', async () => {
		const client = await pgPool.connect()
		try {
			await client.query('BEGIN')

			const { rows } = await client.query('SELECT 1 AS "result"')
			expect(rows[0]['result']).toBe(1)

			await client.query('ROLLBACK')
		} catch (err) {
			throw err
		} finally {
			client.release()
		}
	})
})
