import CustomResponse from '../../../src/models/Response.Model'

const express = require('express')
const supertest = require('supertest')

const app = new express()
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

const route = '/api/products'

//Routes
app.use(require('../../../src/routes'))

app.use(
	express.urlencoded({
		extended: false,
	})
)

describe('Product Scenario', function () {
	let token: string
	beforeAll(async () => {
		const response = await supertest(app)
			.post('/api/users/user/login')
			.type('form')
			.send({
				email: process.env.TEST_USER_EMAIL,
				password: process.env.TEST_USER_PASSWORD,
			})
			.expect(function (res: CustomResponse) {
				app.set('token', res.body.token)
				app.set('user_id', res.body.credentials.id)
			})
	})

	//User adds a product
	test('Add a product', (done) => {
		supertest(app)
			.post(route + '/add')
			.set('Accept', 'application/json')
			.set('Authorization', app.get('token'))
			.send({
				title: 'My awesome Product',
				description: 'My product is awesome',
				image: 'Image',
				created: '2021-12-05 10:00:00',
			})
			.expect(200)
			.expect(function (res: Response | any) {
				res.body.result.user_id = app.get('user_id')
				app.set('product_id', res.body.result.id)
			})
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})

	//User access to products
	test('User fetches products', (done) => {
		supertest(app)
			.get(route + '/')
			.set('Accept', 'application/json')
			.set('Authorization', app.get('token'))
			.expect(200)
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})

	//User fetches its products
	test('User fetches its products', (done) => {
		supertest(app)
			.get(route + '/?user_id=' + app.get('user_id'))
			.set('Accept', 'application/json')
			.set('Authorization', app.get('token'))
			.expect(200)
			.expect(function (res: Response | any) {
				res.body.result[0].user_id = app.get('user_id')
			})
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})

	//User updates its products
	test('User updates its product', (done) => {
		supertest(app)
			.put(route + '/' + app.get('product_id'))
			.set('Accept', 'application/json')
			.set('Authorization', app.get('token'))
			.send({
				title: 'My awesome Product Updated',
				description: 'My product is awesome',
				image: 'Image',
				created: '2021-12-05 10:00:00',
			})
			.expect(200)
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})

	//User deletes its product
	test('User deletes its product', (done) => {
		supertest(app)
			.delete(route + '/' + app.get('product_id'))
			.set('Accept', 'application/json')
			.set('Authorization', app.get('token'))
			.expect(200)
			.end((err: Error, res: Response) => {
				if (err) return done(err)
				return done()
			})
	})
})
